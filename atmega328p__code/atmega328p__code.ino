#include <SoftwareSerial.h>
#include <Servo.h>

Servo servoOne;

int servo_pin = 5

int bt_Tx_pin = 10;
int bt_Rx_pin = 11;

int max_angle = 110;

SoftwareSerial bt_hc05(bt_Tx_pin, bt_Rx_pin);

void setup()
{
    servoOne.attach(servo_pin);

    Serial.begin(9600);
    bt_hc05.begin(9600);

}

void loop()
{
    if(bt_hc05.available() > 0){
        int servo_position = bt_hc05.read();

        if (servo_position < max_angle)
        {
            servoOne.write(servo_position);
        }
    }
}
