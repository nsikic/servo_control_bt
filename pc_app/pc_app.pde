//Učitavanje biblioteka koje ćemo koristiti za komunikaciju putem bluetooth-a
//i biblioteku za klizač kojim ćemo također upravljati servo motorom.
import processing.serial.*; //biblioteka za komunikaciju
import controlP5.*;  //biblioteka za klizač

//Inicijalizacija komunikacije i inicijalizacija klizača
Serial bt_port;
ControlP5 servo_slider;


void setup(){
  //Postavljanje veličine prozora programa
  size(450, 500);

  // Postavljanje port-a na kojemu se nalazi bluetooth kako bi mu omogućili komunikaciju, broj COM port-a ovisi od računala do računala.
  bt_port = new Serial(this, "COM5", 9600); // Započinjemo Serijsku komunikaciju sa portom na kojemu se nalazi upareni uređaj
  bt_port.bufferUntil('\n'); // Očitava sve portove dok ne dođe do novoga reda, to jest /n
  
  //Stvaramo novi klazač objekt
  servo_slider = new ControlP5(this);
  
  //Dodajemo vrijednost klizaču koja će biti poslana Bluetoothom
  servo_slider.addSlider("servo_pos")
  //Postavljamo Poziciju klizača, veličinu, vrijednosti, boju pozadaine, boju odabrane vrijednosti i boju neodabrane vrijednosti
  .setPosition(120,340)
  .setSize(180, 40)
  .setRange(0,108)
  .setValue(0)
  .setColorBackground(color(200,200,200))
  .setColorForeground(color(255,0,0))
  .setColorValue(color(0,0,0))
  .setColorActive(color(200,0,0))
  ;

}

  //Funkcija u Processing-u koja iscrtava definirane karakteristike
void draw(){
  int servo_pos = 0;

  //označavamo veličinu pozadine te joj zadajemo boju
  background(237, 240, 241);
  fill(20, 160, 133);
  stroke(33);
  strokeWeight(1);

  //iscrtavanje pravokutnika koji će predstavljati tipke stanja 1,2 i 3
  rect(150, 100, 150, 25, 10); // stanje 1. 
  rect(150, 140, 150, 25, 10); // stanje 2.
  rect(150, 180, 150, 25, 10); //stanje 3.
  rect(150, 220, 150, 25, 10); //stanje 4.
  rect(150, 260, 150, 25, 10); //stanje 5.
  fill(255);
  //Stavlja tekst, u koordinate gdje se nalaze iscrtan pravokutnici, veličine fonta 15
  textSize(15);
  text("Stanje 1", 200, 118);
  text("Stanje 2", 200, 157);
  text("Stanje 3", 200, 198);
  text("Stanje 4", 200, 240);
  text("Stanje 5", 200, 277);

  // Ako je 1. pravokutnik pritisnut motor se stavlja u 1. poziciju na 0 stupnjeva
  if(mousePressed && mouseX>150 && mouseX<300 && mouseY>100 && mouseY<125){
    
    servo_pos = 0;  //Postavljamo vrijednost varijable koju ćemo slati bluetoothom na 0
    bt_port.write(servo_pos); // Šalje vrijednost 0 koju će arduino pisati na servo motor
    stroke(255,0,0);
    strokeWeight(2);
    noFill();
    rect(150, 100, 150, 25, 10);
  }
 // Ako je 2. pravokutnik pritisnut motor se stavlja u 2. poziciju na 60 stupnjeva
  else if(mousePressed && mouseX>150 && mouseX<300 && mouseY>140 && mouseY<165){
    servo_pos = 27;  //Postavljamo vrijednost varijable koju ćemo slati bluetoothom na 27
    bt_port.write(servo_pos); // Šalje vrijednost 27 koju će arduino pisati na servo motor
    stroke(255,0,0);
    strokeWeight(2);
    noFill();
    rect(150, 140, 150, 25, 10);
  }
  // Ako je 3. pravokutnik pritisnut motor se stavlja u 3. poziciju na 120 stupnjeva
  else if(mousePressed && mouseX>150 && mouseX<300 && mouseY>180 && mouseY<205){
    servo_pos = 55;  //Postavljamo vrijednost varijable koju ćemo slati bluetoothom na 66 
    bt_port.write(servo_pos); // Šalje vrijednost 66 koju će arduino pisati na servo motor
    stroke(255,0,0);
    strokeWeight(2);
    noFill();
    rect(150, 180, 150, 25, 10);
  }
    else if(mousePressed && mouseX>150 && mouseX<300 && mouseY>220 && mouseY<245){  
    servo_pos = 82;  //Postavljamo vrijednost varijable koju ćemo slati bluetoothom na 82 
    bt_port.write(servo_pos); // Šalje vrijednost 82 koju će arduino pisati na servo motor
    stroke(255,0,0);
    strokeWeight(2);
    noFill();
    rect(150, 220, 150, 25, 10);
  }
  
  
  else if(mousePressed && mouseX>150 && mouseX<300 && mouseY>260 && mouseY<285){  
    servo_pos = 109;  //Postavljamo vrijednost varijable koju ćemo slati bluetoothom na 109 
    bt_port.write(servo_pos); // Šalje vrijednost 109 koju će arduino pisati na servo motor
    stroke(255,0,0);
    strokeWeight(2);
    noFill();
    rect(150, 260, 150, 25, 10);
  }

  
}
//funkcija koja očitava vrijednost klizača te šalje željenu vrijednost bluetoothom
void servo_pos(int servo_pos){
  bt_port.write(servo_pos);
}
